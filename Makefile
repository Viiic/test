CC = gcc

all: keeplog

keeplog: keeplog.o keeplog_helper.o listlib.o
	$(CC) -o keeplog keeplog.o keeplog_helper.o listlib.o

keeplog.o: keeplog.c keeplog_helper.h
	$(CC) -c keeplog.c

keeplog_helper.o: keeplog_helper.c listlib.h
	$(CC) -c keeplog_helper.c

listlib.o: listlib.c listlib.h
	$(CC) -c listlib.c

clean:
	@rm *.o keeplog
